package model_data_structures.data_structures.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.google.gson.stream.JsonToken;
import com.opencsv.CSVReader;

import model_data_structures.data_structures.data_structures.IMaxColaPrioridad;
import model_data_structures.data_structures.data_structures.IQueue;
import model_data_structures.data_structures.data_structures.MaxColaPrioridad;
import model_data_structures.data_structures.data_structures.Queue;
import model_data_structures.data_structures.data_structures.RedBlackBST;
import model_data_structures.data_structures.vo.EstadisticasCargaInfracciones;
import model_data_structures.data_structures.vo.InfraccionesFecha;
import model_data_structures.data_structures.vo.InfraccionesFechaHora;
import model_data_structures.data_structures.vo.InfraccionesFranjaHoraria;
import model_data_structures.data_structures.vo.InfraccionesFranjaHorariaViolationCode;
import model_data_structures.data_structures.vo.InfraccionesLocalizacion;
import model_data_structures.data_structures.vo.InfraccionesViolationCode;
import model_data_structures.data_structures.vo.VOMovingViolations;


public class MovingViolationsManager {

	//TODO Definir atributos necesarios
	private IQueue<VOMovingViolations> cola;
	private IMaxColaPrioridad prioridad;
	private String[] semestre;
	private RedBlackBST arbol;
	/**
	 * Metodo constructor
	 */
	public MovingViolationsManager()
	{
		//TODO inicializar los atributos
		cola = new Queue<VOMovingViolations>();
		semestre = new String[6];
		prioridad = new MaxColaPrioridad(20);
		arbol = new RedBlackBST();
	}
	/**
	 * M�todo de retorno con la nomenclatura de los meses
	 * @param pNumsemestre el n�mero del semestre dado
	 * @return semestre, un arreglo con los nombres escogidos
	 */
	public String[] semestre(int pNumsemestre){
		if(pNumsemestre == 1){
			semestre[0] = "January";
			semestre[1] = "February";
			semestre[2] = "March";
			semestre[3] = "April";
			semestre[4] = "May";
			semestre[5] = "June";
		}
		else{
			semestre[0] = "July";
			semestre[1] = "August";
			semestre[2] = "September";
			semestre[3] = "October";
			semestre[4] = "November";
			semestre[5] = "December";
		}
		for(int i = 0; i < semestre.length; i++){
		System.out.println(semestre[i]);
		}
		return semestre;
	}
	/**
	 * Cargar las infracciones de un semestre de 2018
	 * @param numeroSemestre numero del semestre a cargar (1 o 2)
	 * @return objeto con el resultado de la carga de las infracciones
	 */
	public EstadisticasCargaInfracciones loadMovingViolations(int numeroSemestre) {
		// TODO Realizar la carga de infracciones del semestre
		semestre = semestre(numeroSemestre);
		//Locales
		int [] numerofInfracciones = new int[6];
		double [] minimax = new double[4];
		EstadisticasCargaInfracciones retorno = null;
		System.out.println("Solo se leen multas Validas... si tiene Address id vacio se elimina");
		//Lectores
		try {
			CSVReader reader = new CSVReader(new FileReader("data/Moving_Violations_Issued_in_" + semestre[0]+ "_2018.csv" ));
			CSVReader reader1 = new CSVReader(new FileReader("data/Moving_Violations_Issued_in_" + semestre[1]+ "_2018.csv" ));
			CSVReader reader2 = new CSVReader(new FileReader("data/Moving_Violations_Issued_in_" + semestre[2]+ "_2018.csv" ));
			CSVReader reader3 = new CSVReader(new FileReader("data/Moving_Violations_Issued_in_" + semestre[3]+ "_2018.csv" ));
			CSVReader reader4 = new CSVReader(new FileReader("data/Moving_Violations_Issued_in_" + semestre[4]+ "_2018.csv" ));
			CSVReader reader5 = new CSVReader(new FileReader("data/Moving_Violations_Issued_in_" + semestre[5]+ "_2018.csv" ));
		      reader.readNext();
		      while(reader.readNext() != null)
		      {
		    	
		       VOMovingViolations a = creadorObjetos(reader);
		    	cola.enqueue(a);
		    	numerofInfracciones[0] = cola.size();
		    	
		      }
		     
		      reader1.readNext();
		      while(reader1.readNext() != null)
		      {
		    	  VOMovingViolations a = creadorObjetos(reader1);
		    	
		    	
		    	cola.enqueue(a);
		    	numerofInfracciones[1] = cola.size() - numerofInfracciones[0];
		    	
		      }
		   
		      reader2.readNext();
		      while(reader2.readNext() != null)
		      {
		    	
		    	  VOMovingViolations a = creadorObjetos(reader2);
		        numerofInfracciones[2] = cola.size() - numerofInfracciones[1];
		    	
		    	cola.enqueue(a);
		      }
		      
		      reader3.readNext();
		      while(reader3.readNext() != null)
		      {
		    	  
		    	  VOMovingViolations a = creadorObjetos(reader3);
		       
		    	
		    	cola.enqueue(a);
		    	 numerofInfracciones[3] = cola.size() - numerofInfracciones[2];
		      }
		    
		      reader4.readNext();
		      while(reader4.readNext() != null)
		      {
		    	
		    	  VOMovingViolations a = creadorObjetos(reader4);
		    	
		    	
		    	cola.enqueue(a);
		    	numerofInfracciones[4] = cola.size() - numerofInfracciones[3];
		      }
		      
		      reader5.readNext();
		      while(reader5.readNext() != null)
		      {
		    	
		    	  VOMovingViolations a = creadorObjetos(reader5);
		        
		    	
		    	cola.enqueue(a);
		    	numerofInfracciones[5] = cola.size() - numerofInfracciones[4];
		      }
		      retorno = new EstadisticasCargaInfracciones(cola.size(),6,numerofInfracciones,minimax);
		      reader.close();
		      reader1.close();
		      reader2.close();
		      reader3.close();
		      reader4.close();
		      reader5.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Contenedores
		return retorno;
	}
    public VOMovingViolations creadorObjetos(CSVReader reader) throws Exception{
    	
    	String Location="";
		String TotalPaid="";
		String AccidentIndicator="";
		LocalDateTime TIDate=null;
		String ViolationC="";
		String FAMT="";
		Integer Street_Id=0;
		Integer Address_Id=0;
		String X="";
		String Y="";
		String P1="";
		String ObjectId="";
		String P2="";
		String ViolationDescription="";
		//Crear el objeto con la informaci�n de una multa...
		
		 String[] linea;
	      reader.readNext();
	      while((linea = reader.readNext()) != null){
			//Me aseguro de que coja un objeto v�lido
				
					ObjectId= linea[0];
				
				
					Location = linea[2];
				
				
					X= linea[5];
				
				
					Y= linea[6];
				
				
					try 
					{
						Address_Id = Integer.parseInt(linea[3]);
					} 
					catch (Exception e) 
					{
						//Si no es un int lo transforma en -10 para limpiar la base
						Address_Id=-10;
					}
					
				 
				
					try 
					{
						Street_Id = Integer.parseInt(linea[4]);
					} 
					catch (Exception e) 
					{
						//Si no es un int lo transforma en -10 para limpiar la base
						Street_Id=-10;
					}
				
				
					ViolationDescription = linea[14];
				
				
					TotalPaid = linea[8];
				
				
					AccidentIndicator = linea[12];
				
				
					TIDate = ManejoFechaHora.convertirFecha_Hora_LDT(linea[13]);
				
				
					ViolationC = linea[13];
				
				
					FAMT = linea[9];
				
				
					P1 = linea[10];
				
				
					try 
					{
						P2 = linea[11];
					} 
					catch (Exception e) 
					{
						P2 = "P2";
						//DO NOTHING
					}
				
				//Si el objeto no cumple los requisitos para ser JsonObject
				
					
			//Salta al siguiente si est� vacio
			
		}
		
		//Llenamos el objeto con la info dada
		VOMovingViolations object =  new VOMovingViolations(Integer.parseInt(ObjectId),Location, Address_Id, Street_Id, Double.parseDouble(X), Double.parseDouble(Y),Double.parseDouble(FAMT),Double.parseDouble(TotalPaid), P1,P2,AccidentIndicator,TIDate,ViolationC,ViolationDescription);
		return object;
    	
    }
	/**
	  * Requerimiento 1A: Obtener el ranking de las N franjas horarias
	  * que tengan m�s infracciones. 
	  * @param int N: N�mero de franjas horarias que tienen m�s infracciones
	  * @return Cola con objetos InfraccionesFranjaHoraria
	  */
	public IQueue<InfraccionesFranjaHoraria> rankingNFranjas(int N)
	{
		// TODO completar
		return null;		
	}
	
	/**
	  * Requerimiento 2A: Consultar  las  infracciones  por
	  * Localizaci�n  Geogr�fica  (Xcoord, Ycoord) en Tabla Hash.
	  * @param  double xCoord : Coordenada X de la localizacion de la infracci�n
	  *			double yCoord : Coordenada Y de la localizacion de la infracci�n
	  * @return Objeto InfraccionesLocalizacion
	  */
	public InfraccionesLocalizacion consultarPorLocalizacionHash(double xCoord, double yCoord)
	{
		// TODO completar
		return null;		
	}
	
	/**
	  * Requerimiento 3A: Buscar las infracciones por rango de fechas
	  * @param  LocalDate fechaInicial: Fecha inicial del rango de b�squeda
	  * 		LocalDate fechaFinal: Fecha final del rango de b�squeda
	  * @return Cola con objetos InfraccionesFecha
	  */
	public IQueue<InfraccionesFecha> consultarInfraccionesPorRangoFechas(LocalDate fechaInicial, LocalDate fechaFinal)
	{
		// TODO completar
		return null;		
	}
	
	/**
	  * Requerimiento 1B: Obtener  el  ranking  de  las  N  tipos  de  infracci�n
	  * (ViolationCode)  que  tengan  m�s infracciones.
	  * @param  int N: Numero de los tipos de ViolationCode con m�s infracciones.
	  * @return Cola con objetos InfraccionesViolationCode con top N infracciones
	  */
	public IQueue<InfraccionesViolationCode> rankingNViolationCodes(int N)
	{
		// TODO completar
		InfraccionesViolationCode instancia = null;
		IQueue<InfraccionesViolationCode> retorno = new Queue<InfraccionesViolationCode>();
		for(int i = 0; i < N; i++){
		 instancia = new InfraccionesViolationCode(cola.dequeue().getViolationCode(), cola);
		}
		retorno.enqueue(instancia);
		return retorno;		
	}

	
	/**
	  * Requerimiento 2B: Consultar las  infracciones  por  
	  * Localizaci�n  Geogr�fica  (Xcoord, Ycoord) en Arbol.
	  * @param  double xCoord : Coordenada X de la localizacion de la infracci�n
	  *			double yCoord : Coordenada Y de la localizacion de la infracci�n
	  * @return Objeto InfraccionesLocalizacion
	  */
	public InfraccionesLocalizacion consultarPorLocalizacionArbol(double xCoord, double yCoord)
	{
		// TODO completar
		
		
		InfraccionesLocalizacion retorno = null;
		for(int i = 0; i < cola.size(); i++){
		retorno = new InfraccionesLocalizacion(xCoord, yCoord, cola.dequeue().getLocation(), cola.dequeue().getAddressId(), cola.dequeue().getStreetSegId(), cola);
		arbol.put((Comparable) arbol.keys(xCoord, yCoord), cola.dequeue());
		}
		return retorno;		
	}
	
	/**
	  * Requerimiento 3B: Buscar las franjas de fecha-hora donde se tiene un valor acumulado
	  * de infracciones en un rango dado [US$ valor inicial, US$ valor final]. 
	  * @param  double valorInicial: Valor m�nimo acumulado de las infracciones
	  * 		double valorFinal: Valor m�ximo acumulado de las infracciones.
	  * @return Cola con objetos InfraccionesFechaHora
	  */
	public IQueue<InfraccionesFechaHora> consultarFranjasAcumuladoEnRango(double valorInicial, double valorFinal)
	{
		// TODO completar
		IQueue<InfraccionesFechaHora> retorno = new Queue<InfraccionesFechaHora>();
		InfraccionesFechaHora a = null;
		
		for(int i = 0; i < cola.size(); i++){
			VOMovingViolations que = cola.dequeue();
			double num = que.getFineAmount();
			if(num < valorFinal && valorInicial < num){
			a = new InfraccionesFechaHora(que.getTicketIssueDate(), que.getTicketIssueDate(), cola);	
			}
		}
			retorno.enqueue(a);
		
		return retorno;		
	}
	
	/**
	  * Requerimiento 1C: Obtener  la informaci�n de  una  addressId dada
	  * @param  int addressID: Localizaci�n de la consulta.
	  * @return Objeto InfraccionesLocalizacion
	  */
	public InfraccionesLocalizacion consultarPorAddressId(int addressID)
	{
		// TODO completar
		//Arbol BST
		RedBlackBST<Integer,VOMovingViolations> arbol1 = new RedBlackBST<Integer,VOMovingViolations>();
		for (int i = 0; i < cola.size(); i++){
			VOMovingViolations r = cola.dequeue();
			arbol1.put(r.getAddressId(), r);
		}
		InfraccionesLocalizacion retorno = null;
		VOMovingViolations y = arbol1.buscarNodo(addressID).darValor();
		retorno = new InfraccionesLocalizacion(y.getxCoord(), y.getyCoord(), y.getLocation(), y.getAddressId(), y.getStreetSegId(), cola);
		return retorno;		
	}
	
	/**
	  * Requerimiento 2C: Obtener  las infracciones  en  un  rango de
	  * horas  [HH:MM:SS  inicial,HH:MM:SS  final]
	  * @param  LocalTime horaInicial: Hora  inicial del rango de b�squeda
	  * 		LocalTime horaFinal: Hora final del rango de b�squeda
	  * @return Objeto InfraccionesFranjaHorariaViolationCode
	  */
	public InfraccionesFranjaHorariaViolationCode consultarPorRangoHoras(LocalTime horaInicial, LocalTime horaFinal)
	{
		// TODO completar
		InfraccionesFranjaHorariaViolationCode retorno = null;
		RedBlackBST<String,VOMovingViolations> arbol1 = new RedBlackBST<String,VOMovingViolations>();
		for (int i = 0; i < cola.size(); i++){
			VOMovingViolations r = cola.dequeue();
			arbol1.put(r.getViolationCode(), r);
		}
		retorno = new InfraccionesFranjaHorariaViolationCode(horaInicial, horaFinal, cola, this.rankingNViolationCodes(arbol1.height()) );
		return retorno;		
	}
	
	/**
	  * Requerimiento 3C: Obtener  el  ranking  de  las  N localizaciones geogr�ficas
	  * (Xcoord,  Ycoord)  con  la mayor  cantidad  de  infracciones.
	  * @param  int N: Numero de las localizaciones con mayor n�mero de infracciones
	  * @return Cola de objetos InfraccionesLocalizacion
	  */
	public IQueue<InfraccionesLocalizacion> rankingNLocalizaciones(int N)
	{
		// TODO completar
		return null;		
	}
	
	/**
	  * Requerimiento 4C: Obtener la  informaci�n  de  los c�digos (ViolationCode) ordenados por su numero de infracciones.
	  * @return Contenedora de objetos InfraccionesViolationCode.
	  // TODO Definir la estructura Contenedora
	  */
	public Contenedora<InfraccionesViolationCode> ordenarCodigosPorNumeroInfracciones()
	{
		// TODO completar
		// TODO Definir la Estructura Contenedora
		return null;		
	}


}
