package model_data_structures.data_structures.data_structures;

public class MaxColaPrioridad<T extends Comparable<T>> implements IMaxColaPrioridad<T> {


	private Object[] maxColaPrioridad;

	int numElementos;
	public MaxColaPrioridad(int prioridadMax) 
	{
		maxColaPrioridad = new Object [prioridadMax];
		numElementos =0;		
	}
	public int darNumElementos() {
		
		return numElementos;
	}

	
	public void agregar(T elemento) {
		
		maxColaPrioridad[numElementos++]=((T) elemento);

	}


	public T delMax() {
		
		int max = 0;
		for(int i = 0; i<numElementos; i++)
		{
			if(less(max, i))
			{
				max = i;
			}
			exch(max, numElementos-1);
		}
		return (T) maxColaPrioridad[numElementos--];
	}

	
	public T max() {
		
		int max= 0;

		if(numElementos != 0)
		{ for(int i = 0; i<numElementos; i++)
		{
			if(less(max, i))
			{
				max = i;
			}
		}
		}
		return (T) maxColaPrioridad[max];
}

	
	public boolean esVacia() {
		
		return numElementos==0;
	}
	private boolean less(int i, int j)
	{
		return ((T) maxColaPrioridad[i]).compareTo((T) maxColaPrioridad[j]) < 0;

	}
	private void exch(int i, int j)
	{
		Object t = maxColaPrioridad[i]; 
		maxColaPrioridad[i] = maxColaPrioridad[j]; 
		maxColaPrioridad[j] = t; 
	}


}
