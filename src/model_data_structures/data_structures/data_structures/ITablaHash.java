package model_data_structures.data_structures.data_structures;

import java.util.Iterator;

public interface ITablaHash<K extends Comparable<K>, V> {

	public void put(K key, V value);
	
	public V get(K key);
	
	public V delete(K key);
	
	public Iterator<K> iterator();
}