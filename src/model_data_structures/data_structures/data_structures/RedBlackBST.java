package model_data_structures.data_structures.data_structures;
import java.util.Iterator;

import model_data_structures.data_structures.data_structures.Node.Color;




public class RedBlackBST<K extends Comparable<K>, V> implements IRedBlackBST<K, V> {

	
	private Node<K,V> raiz;

	public RedBlackBST() {
		raiz = null;
	}

	public RedBlackBST(Node<K,V> raiz) {
		this.raiz = raiz;
	}

	public Node<K,V> darRaiz() {
		return raiz;
	}

	
	public void put(K key, V value) {
		if (raiz == null) {
			raiz = new Node<K,V>(key, value, null);
		} else {
			raiz = raiz.insertar(key, value);
		}

		raiz.cambiarColor(Color.BLACK);
	}

	public Node<K, V> buscarNodo(K key) {
		if(raiz == null) {
			return null;
		} else {
			return raiz.buscar(key);
		}
	}

	
	public V get(K key) {
		if(raiz == null) {
			return null;
		} else {
			Node<K,V> nodo = raiz.buscar(key);
			if(nodo != null) {
				return nodo.value;
			} else {
				return null;
			}
		}
	}

	
	public K min() {
		if(raiz == null) {
			return null;
		} else {
			return raiz.minimum().key;
		}
	}

	@Override
	public K max() {
		if(raiz == null) {
			return null;
		} else {
			return raiz.maximum().key;
		}
	}

	@Override
	public int size() {
		if(raiz == null) {
			return 0;
		} else {
			return raiz.darPeso();
		}
	}

	@Override
	public int height() {
		if(raiz == null) {
			return 0;
		} else {
			return raiz.darAltura();
		}
	}

	@Override
	public boolean isEmpty() {
		return raiz == null;
	}

	@Override
	public int getHeight(K key) {
		if(raiz == null) {
			return 0;
		} else {
			return raiz.darAltura(key);
		}
	}

	@Override
	public boolean contains(K key) {
		if(raiz == null) {
			return false;
		} else {
			return raiz.contains(key);
		}
	}

	
	
	public Iterator<K> keys() {
        if (isEmpty()) return new Queue<K>().iterator();
        return keys(min(), max());
    }
	
    public Iterator<K> keys(K lo, K hi) {
        if (lo == null) throw new IllegalArgumentException("first argument to keys() is null");
        if (hi == null) throw new IllegalArgumentException("second argument to keys() is null");

        Queue<K> queue = new Queue<K>();
        
        keys(raiz, queue, lo, hi);
        return queue.iterator();
    } 

    private void keys(Node<K,V> x, Queue<K> queue, K lo, K hi) { 
        if (x == null) return; 
        int cmplo = lo.compareTo(x.key); 
        int cmphi = hi.compareTo(x.key); 
        if (cmplo < 0) keys(x.izquierda, queue, lo, hi); 
        if (cmplo <= 0 && cmphi >= 0) queue.enqueue(x.key); 
        if (cmphi > 0) keys(x.derecha, queue, lo, hi); 
    }
    
    private void values(Node<K,V> x, Queue<V> queue, K lo, K hi) { 
        if (x == null) return; 
        int cmplo = lo.compareTo(x.key); 
        int cmphi = hi.compareTo(x.key); 
        if (cmplo < 0) values(x.izquierda, queue, lo, hi); 
        if (cmplo <= 0 && cmphi >= 0) queue.enqueue(x.value); 
        if (cmphi > 0) values(x.derecha, queue, lo, hi); 
    }

	
	public Iterator<V> valuesInRange(K init, K end) {
		Queue<V> values = new Queue<>();
		values(raiz, values, init, end);
		return values.iterator();
	}

	
	public Iterator<K> keysInRange(K init, K end) {
		return keys(init, end);
	}

}
