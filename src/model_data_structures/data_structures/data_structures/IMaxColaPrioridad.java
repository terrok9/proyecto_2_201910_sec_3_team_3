package model_data_structures.data_structures.data_structures;

public interface IMaxColaPrioridad<T>  {

	public int darNumElementos();

	public void agregar(T elemento);

	public T delMax();

	public T max();

	public boolean esVacia();
}

