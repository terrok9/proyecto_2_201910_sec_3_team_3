package model_data_structures.data_structures.data_structures;

public class Node<K extends Comparable<K>, V> {

	public final static String IZQUIERDA = "Izquierda";
	public final static String DERECHA = "Derecha";

	public enum Color {
		RED,
		BLACK
	}

	Color color;
	K key;
	V value;
	Node<K,V> izquierda, derecha, padre;

	public Node(K key, V value, Node<K,V> padre) {
		this.color = Color.RED;
		this.key = key;
		this.value = value;
		this.padre = padre;
		izquierda = null;
		derecha = null;
	}

	public V darValor() {
		return value;
	}

	public Node<K,V> darHijoIzquierda() {
		return izquierda;
	}

	public Node<K,V> darHijoDerecha() {
		return derecha;
	}

	public Node<K,V> darPadre() {
		return padre;
	}

	
	public void cambiarColor(Color color) {
		this.color = color;
	}

	public boolean esEnlanceRojo(Node<K,V> nodo) {
		return nodo != null && nodo.color == Color.RED;
	}

	public boolean esEnlaceRojo() {
		return this.color == Color.RED;
	}

	private void flipColors() {
		izquierda.color = Color.BLACK;
		derecha.color = Color.BLACK;
		color = Color.RED;
	}

	private Node<K,V> rotateLeft() {
		Node<K,V> hermano = derecha;
		derecha = hermano.izquierda;
		hermano.izquierda = this;
		hermano.color = this.color;
		this.color = Color.RED;

		return hermano;
	}

	private Node<K,V> rotateRight() {
		Node<K,V> hermano = izquierda;
		izquierda = hermano.derecha;
		hermano.derecha = this;
		hermano.color = this.color;
		this.color = Color.RED;

		return hermano;
	}

	public Node<K,V> insertar(K key, V value) {
		if (this.key.compareTo(key) == 0) {
			this.value = value;
			return this;

		} else if (this.key.compareTo(key) > 0) {
			if (izquierda == null) {
				izquierda = new Node<K,V>(key, value, this);
			} else {
				izquierda = izquierda.insertar(key, value);
			}

		} else {
			if (derecha == null) {
				derecha = new Node<K,V>(key, value, this);
			} else {
				derecha = derecha.insertar(key, value);
			}
		}

		
		Node<K,V> h = this;
		if (esEnlanceRojo(h.derecha) && !esEnlanceRojo(h.izquierda)) h = h.rotateLeft();
		if (h.izquierda != null && esEnlanceRojo(h.izquierda) && esEnlanceRojo(h.izquierda.izquierda)) h = h.rotateRight();
		if (esEnlanceRojo(h.izquierda) && esEnlanceRojo(h.derecha)) h.flipColors();

		return h;
	}

	public Node<K,V> buscar(K key) {
		Node<K,V> nodo = null;

		if (this.key.compareTo(key) == 0) {
			nodo = this;
		} else if (this.izquierda != null && this.key.compareTo(key) > 0) {
			nodo = izquierda.buscar(key);
		} else if (this.derecha != null) {
			nodo = derecha.buscar(key);
		}

		return nodo;
	}

	
	public Node<K,V> minimum() {
		if(this.izquierda == null) {
			return this;
		} else {
			return this.izquierda.minimum();
		}
	}

	public Node<K,V> maximum() {
		if(this.derecha == null) {
			return this;
		} else {
			return this.derecha.maximum();
		}
	}


	public int darPeso() {
		int peso = 1;

		if(izquierda != null) {
			peso += izquierda.darPeso();
		}

		if(derecha != null) {
			peso += derecha.darPeso();
		}

		return peso;
	}

	public int darAltura() {
		return darAltura(this);
	}

	private int darAltura(Node<K,V> nodo) {
		if(nodo == null || esEnlanceRojo(nodo)) return -1;
		return 1 + Math.max(darAltura(nodo.derecha), darAltura(nodo.izquierda));
	}

	public int darAltura(K key) {
		Node<K, V> nodo = buscar(key);

		return darAltura(nodo);
	}

	public boolean esHoja() {
		return this.derecha == null && this.izquierda == null;
	}

	public boolean contains(K key) {
		boolean contains = false;

		if(this.key.compareTo(key) == 0) {
			contains = true;
		} else if(this.izquierda != null && this.key.compareTo(key) > 0) {
			contains = this.izquierda.contains(key);
		} else if(this.derecha != null) {
			contains = this.derecha.contains(key);
		}
		return contains;
	}

}

